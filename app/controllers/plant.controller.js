const db = require("../models");
const Plant = db.plants;
const Op = db.Sequelize.Op;

exports.findAll = (req, res) => {
  const title = req.query.title;
  var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  Plant.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving plants.",
      });
    });
};

exports.create = (req, res) => {
  if (!req.body.name || !req.body.pot || !req.body.adopted) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
    return;
  }

  const plant = {
    name: req.body.name,
    pot: req.body.pot,
    adopted: req.body.adopted,
  };

  Plant.create(plant)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Plant.",
      });
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  Plant.update(req.body, { where: { id: id } })
    .then((num) => {
      if (num === 1) {
        res.send({
          message: "Plant was updated succesfully.",
        });
      } else {
        res.send({ message: `Cannot update Plant with id: ${id}.` });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while updating the Plant.",
      });
    });
};
