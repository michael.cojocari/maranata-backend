module.exports = (sequelize, Sequelize) => {
    const Plant = sequelize.define("plants", {
        name: {
            type: Sequelize.STRING
        },
        pot: {
            type: Sequelize.STRING
        },
        adopted: {
            type: Sequelize.DATE
        },
        end_of_life: {
            type: Sequelize.DATE
        }
    });

    return Plant;
}
