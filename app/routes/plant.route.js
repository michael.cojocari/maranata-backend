module.exports = app => {
    const plants = require('../controllers/plant.controller.js')
    var router = require('express').Router();

    router.get('/', plants.findAll);
    router.post('/', plants.create);
    router.put('/:id', plants.update);

    app.use('/plants', router);
}
